# Traefik 2 Docker Swarm Setup

A simple easy to use docker swarm setup for Traefik. This should allow you to plug and play it into your existing stack with few modifications.

There's a nice explanation of this setup [here](https://blog.creekorful.com/how-to-install-traefik-2-docker-swarm).
