#!/bin/bash

docker network create --driver=overlay traefik-public

docker-compose -f traefik.yml -f hello-world.yml config > docker-stack.yml

docker stack deploy -c docker-stack.yml helloworld

